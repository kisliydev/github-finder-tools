import React   from 'react';
import spinner from './spinner.gif'

const Style = { width: '200px', margin: 'auto', display: 'block' };

const Spinner = () => <img src={spinner} alt="loading..." style={Style}/>;

export default Spinner;